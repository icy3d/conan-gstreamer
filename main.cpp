#include <gst/gst.h>
#include <gst/gstplugin.h>
#include <iostream>

// check this page if you're not sure what element resides in what plugin
// https://thiblahute.github.io/GStreamer-doc/plugins.html?gi-language=c

#ifdef GST_PLUGINS_GOOD_STATIC
extern "C" {
GST_PLUGIN_STATIC_DECLARE(coreelements);
GST_PLUGIN_STATIC_DECLARE(videoconvert);
GST_PLUGIN_STATIC_DECLARE(autodetect);
GST_PLUGIN_STATIC_DECLARE(wavparse);
GST_PLUGIN_STATIC_DECLARE(proxy);
}
#endif

void registerPlugins() {
#ifdef GST_PLUGINS_GOOD_STATIC
  GST_PLUGIN_STATIC_REGISTER(coreelements);
  GST_PLUGIN_STATIC_REGISTER(videoconvert);
  GST_PLUGIN_STATIC_REGISTER(autodetect);
  GST_PLUGIN_STATIC_REGISTER(wavparse);
  GST_PLUGIN_STATIC_REGISTER(proxy);
#endif
}

GstElement *loadPlugin(const char *name) {
  GstElement *plugin = gst_element_factory_make(name, nullptr);
  if (!plugin) {
    std::cerr << "failed to create " << name << " element" << std::endl;
  } else {
    std::cout << name << " has been created successfully" << std::endl;
  }
  return plugin;
}

int main(int argc, char **argv) {
  //  gst_debug_set_active(TRUE);
  //  gst_debug_set_default_threshold(GST_LEVEL_INFO);

  gst_init(nullptr, nullptr);

  registerPlugins();

  GstElement *pipeline = gst_pipeline_new("test-pipeline");
  if (pipeline)
    gst_object_unref(GST_OBJECT(pipeline));

  GstElement *fakesrc = loadPlugin("fakesrc");
  if (fakesrc)
    gst_object_unref(GST_OBJECT(fakesrc));

  // base plugin
  GstElement *videoconvert = loadPlugin("videoconvert");
  if (videoconvert)
    gst_object_unref(GST_OBJECT(videoconvert));

  // good plugin
  GstElement *autovideosrc = loadPlugin("autovideosrc");
  if (autovideosrc)
    gst_object_unref(GST_OBJECT(autovideosrc));

  GstElement *wavparse = loadPlugin("wavparse");
  if (wavparse)
    gst_object_unref(GST_OBJECT(wavparse));

  // bad plugin
  GstElement *proxysink = loadPlugin("proxysink");
  if (proxysink)
    gst_object_unref(GST_OBJECT(proxysink));

  return EXIT_SUCCESS;
}
