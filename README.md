# Build

```bash
mkdir build && cd build
conan install .. -b missing
cmake ..
make
```

# Run

```bash
./bin/conan-gstreamer
```